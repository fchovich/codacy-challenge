package codacy.challenge.commitretriever;

import codacy.challenge.vcs.ImmutableListCommitOptions;
import codacy.challenge.vcs.ListCommitOptions;
import codacy.challenge.vcs.RepositoryAlreadyExistsException;
import codacy.challenge.vcs.VCSCaller;
import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.domain.ImmutableCommit;
import codacy.challenge.vcs.domain.ImmutableRepository;
import codacy.challenge.vcs.domain.Repository;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import static codacy.challenge.commitretriever.CommitRetriever.BASE_PATH;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@SuppressWarnings("UnusedLabel") // USED FOR BDD BLOCKS
public class CommitRetrieverTest {

    private VCSCaller vcsAPI;

    private VCSCaller vcsCLI;

    private CommitRetriever commitRetriever;

    @Before
    public void setup() {
        this.vcsAPI = mock(VCSCaller.class, "API CALLER");
        this.vcsCLI = mock(VCSCaller.class, "CLI CALLER");
        this.commitRetriever = new CommitRetriever(this.vcsAPI, this.vcsCLI);
    }

    @After
    public void verifyMocks() {
        Mockito.verifyNoMoreInteractions(this.vcsAPI, this.vcsCLI);
    }

    @Test
    public void successfullyCallsAPI() throws RepositoryAlreadyExistsException {
        final String owner, remoteName;
        final CommitRetrieverOptions options;
        final Repository repository;
        final ListCommitOptions listOptions;

        final List<Commit> expectedCommits, actualCommits;

        given:
        options = ImmutableCommitRetrieverOptions.builder().build();
        owner = "fchovich";
        remoteName = "codacy-challenge";

        orchestrate:
        repository = ImmutableRepository.builder()
                .owner(owner)
                .name(remoteName)
                .localPath(Paths.get("/tmp"))
                .build();

        listOptions = ImmutableListCommitOptions.builder()
                .page(options.getPage())
                .pageSize(options.getSize())
                .build();

        expectedCommits = Collections.singletonList(
                ImmutableCommit.builder()
                        .hash("1")
                        .commiterEmail("a@gmail.com")
                        .date(1L).build()
        );

        when(this.vcsCLI.clone(BASE_PATH, owner, remoteName, remoteName)).thenReturn(repository);
        when(this.vcsAPI.listCommits(repository, listOptions)).thenReturn(expectedCommits);

        when:
        actualCommits = this.commitRetriever.listCommits(owner, remoteName, options);

        then:
        Assertions.assertThat(actualCommits)
                .as("The commits returned by the UUT should be the returned by the API caller.")
                .hasSize(1)
                .isEqualTo(expectedCommits);

        and:
        verify(this.vcsAPI, only()).listCommits(repository, listOptions);
        verify(this.vcsCLI, only()).clone(BASE_PATH, owner, remoteName, remoteName);
    }

    @Test
    public void successfullyCallsCLI() throws RepositoryAlreadyExistsException {
        final String owner, remoteName;
        final CommitRetrieverOptions options;
        final Repository repository;
        final ListCommitOptions listOptions;

        final List<Commit> expectedCommits, actualCommits;

        given:
        options = ImmutableCommitRetrieverOptions.builder().build();
        owner = "fchovich";
        remoteName = "codacy-challenge";

        orchestrate:
        repository = ImmutableRepository.builder()
                .owner(owner)
                .name(remoteName)
                .localPath(Paths.get("/tmp"))
                .build();

        listOptions = ImmutableListCommitOptions.builder()
                .page(options.getPage())
                .pageSize(options.getSize())
                .build();

        expectedCommits = Collections.singletonList(
                ImmutableCommit.builder()
                        .hash("1")
                        .commiterEmail("a@gmail.com")
                        .date(1L).build()
        );

        when(this.vcsAPI.listCommits(repository, listOptions)).thenThrow(Exception.class);
        when(this.vcsCLI.clone(BASE_PATH, owner, remoteName, remoteName)).thenReturn(repository);
        when(this.vcsCLI.listCommits(repository, listOptions)).thenReturn(expectedCommits);

        when:
        actualCommits = this.commitRetriever.listCommits(owner, remoteName, options);

        then:
        Assertions.assertThat(actualCommits)
                .as("The commits returned by the UUT should be the returned by the CLI caller.")
                .hasSize(1)
                .isEqualTo(expectedCommits);

        and:
        verify(this.vcsAPI, times(1)).listCommits(repository, listOptions);
        verify(this.vcsCLI, times(1)).clone(BASE_PATH, owner, remoteName, remoteName);
        verify(this.vcsCLI, times(1)).listCommits(repository, listOptions);
    }

    @Test
    public void localRepositoryAlreadyExists() throws RepositoryAlreadyExistsException {
        final String owner, remoteName;
        final CommitRetrieverOptions options;
        final Repository repository;
        final ListCommitOptions listOptions;

        final List<Commit> expectedCommits, actualCommits;

        given:
        options = ImmutableCommitRetrieverOptions.builder().build();
        owner = "fchovich";
        remoteName = "codacy-challenge";

        orchestrate:
        repository = ImmutableRepository.builder()
                .owner(owner)
                .name(remoteName)
                .localPath(BASE_PATH.resolve(remoteName))
                .build();

        listOptions = ImmutableListCommitOptions.builder()
                .page(options.getPage())
                .pageSize(options.getSize())
                .build();

        expectedCommits = Collections.singletonList(
                ImmutableCommit.builder()
                        .hash("1")
                        .commiterEmail("a@gmail.com")
                        .date(1L).build()
        );

        when(this.vcsCLI.clone(BASE_PATH, owner, remoteName, remoteName))
                .thenThrow(new RepositoryAlreadyExistsException(BASE_PATH.resolve(remoteName), "Error"));
        when(this.vcsAPI.listCommits(repository, listOptions)).thenReturn(expectedCommits);

        when:
        actualCommits = this.commitRetriever.listCommits(owner, remoteName, options);

        then:
        Assertions.assertThat(actualCommits)
                .as("The commits returned by the UUT should be the returned by the API caller.")
                .hasSize(1)
                .isEqualTo(expectedCommits);

        and:
        verify(this.vcsCLI, only()).clone(BASE_PATH, owner, remoteName, remoteName);
        verify(this.vcsAPI, only()).listCommits(repository, listOptions);
    }

}
