package codacy.challenge.commitretriever;

import org.immutables.value.Value;


/**
 * The {@link CommitRetriever} options.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@Value.Immutable
public interface CommitRetrieverOptions {

    /**
     * The number of commits to retrieve.
     *
     * @return The number of commits to retrieve.
     */
    @Value.Default
    default int getSize() {
        return 100;
    }

    /**
     * The page of retrieve to commits.
     *
     * @return The page of commits to retrieve.
     */
    @Value.Default
    default int getPage() {
        return 1;
    }
}
