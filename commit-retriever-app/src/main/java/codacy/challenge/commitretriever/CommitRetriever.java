package codacy.challenge.commitretriever;

import codacy.challenge.vcs.ImmutableListCommitOptions;
import codacy.challenge.vcs.ListCommitOptions;
import codacy.challenge.vcs.RepositoryAlreadyExistsException;
import codacy.challenge.vcs.VCSCaller;
import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.domain.ImmutableRepository;
import codacy.challenge.vcs.domain.Repository;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * The commit retriever main class.
 * <p>
 * Provides all functionality in the highest level outside of user interfaces.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public class CommitRetriever {

    /**
     * The logger for this class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CommitRetriever.class);

    /**
     * The {@link VCSCaller} for API operations.
     */
    private final VCSCaller vcsApi;

    /**
     * The {@link VCSCaller} for CLI operations.
     */
    private final VCSCaller vcsCli;

    /**
     * The base path in which repositories are cloned.
     */
    // TODO fchovich setup another type of configuration
    static final Path BASE_PATH = Paths.get(System.getProperty("user.home"), ".commitretriever");

    /**
     * Default constructor.
     *
     * @param vcsApi The {@link VCSCaller} for API operations.
     * @param vcsCli The {@link VCSCaller} for CLI operations.
     */
    public CommitRetriever(final VCSCaller vcsApi, final VCSCaller vcsCli) {
        this.vcsApi = vcsApi;
        this.vcsCli = vcsCli;
    }

    /**
     * List all commits for the given owner, name and options.
     * <p>
     * Mainly:
     * <ul>
     * <li>Clones the repository locally if internet provided</li>
     * <li>Calls the public REST API to list commits</li>
     * <li>If REST API operation fails (due to a network error), the CLI is called as a fallback</li>
     * </ul>
     *
     * @param owner   The owner of the repository.
     * @param name    The name of the repository.
     * @param options The options to retrieve commits.
     * @return The list of commits retrieved.
     */
    public List<Commit> listCommits(final String owner,
                                    final String name,
                                    final CommitRetrieverOptions options) {
        Preconditions.checkArgument(StringUtils.isNotBlank(owner), "The owner of the repository is mandatory.");
        Preconditions.checkArgument(StringUtils.isNotBlank(name), "The name of the repository is mandatory.");

        final Repository repository = getRepository(this.vcsCli, BASE_PATH, owner, name);

        try {
            LOGGER.debug("Calling the VCS API for {}/{}.", repository.getOwner(), repository.getName());

            return this.vcsApi.listCommits(repository, toListOptions(options));
        } catch (Exception e) {
            LOGGER.debug("Calling the VCS CLI for {}/{}.", repository.getOwner(), repository.getName());

            return this.vcsCli.listCommits(repository, toListOptions(options));
        }
    }

    /**
     * Gets the repository. It will try to clone the repository but if local repository already exists it simply returns
     * the repository instance.
     *
     * @param cli      The {@link VCSCaller} for CLI operations.
     * @param basePath The base path to clone the repository in.
     * @param owner    The owner of the repository.
     * @param name     The name of the repository.
     * @return The repository.
     */
    private static Repository getRepository(final VCSCaller cli,
                                            final Path basePath,
                                            final String owner,
                                            final String name) {
        try {
            // TODO fchovich fetch to keep this updated
            return cli.clone(basePath, owner, name, name);
        } catch (RepositoryAlreadyExistsException e) {
            return ImmutableRepository.builder()
                    .owner(owner)
                    .name(name)
                    .localPath(e.getRepositoryPath())
                    .build();
        }
    }

    /**
     * Creates the {@link ListCommitOptions} from the {@link CommitRetrieverOptions}.
     *
     * @param options The options to convert.
     * @return The converted options.
     */
    private static ListCommitOptions toListOptions(final CommitRetrieverOptions options) {
        return ImmutableListCommitOptions.builder()
                .page(options.getPage())
                .pageSize(options.getSize())
                .build();
    }
}
