package codacy.challenge.vcs;

import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.domain.Repository;

import java.nio.file.Path;
import java.util.List;

/**
 * An interface for any version control system operation.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public interface VCSCaller {

    /**
     * Clone the repository.
     *
     * @param basePath   The base path to clone.
     * @param owner      The owner of the repository.
     * @param remoteName The remote name of the repository.
     * @param localName  The local name of the repository.
     * @return The repository cloned.
     * @throws RepositoryAlreadyExistsException If the repository is already cloned.
     */
    Repository clone(final Path basePath,
                     final String owner,
                     final String remoteName,
                     final String localName) throws RepositoryAlreadyExistsException;

    /**
     * List commits for the given repository and the given options.
     *
     * @param repository The repository to list commits from.
     * @param options    The options to list the commits.
     * @return The list of commits.
     */
    List<Commit> listCommits(final Repository repository, final ListCommitOptions options);

    /**
     * Updates the given repository.
     *
     * @param repository The repository to update.
     */
    default void update(final Repository repository) {
        //TODO fchovich KEEP LOCAL REPO UPDATED
        throw new UnsupportedOperationException("Update not supported yet.");
    }
}
