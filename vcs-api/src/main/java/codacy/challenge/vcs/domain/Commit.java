package codacy.challenge.vcs.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * The commit representation.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@Value.Immutable
@JsonSerialize(as = ImmutableCommit.class)
@JsonDeserialize(as = ImmutableCommit.class)
public interface Commit {

    /**
     * The commit hash.
     *
     * @return The commit hash.
     */
    @JsonProperty
    String getHash();

    /**
     * The commiter email.
     *
     * @return The commiter email.
     */
    @JsonProperty
    String getCommiterEmail();

    /**
     * The date of the commit in epoch millis.
     *
     * @return The date of the commit in epoch millis.
     */
    @JsonProperty
    Long getDate();
}
