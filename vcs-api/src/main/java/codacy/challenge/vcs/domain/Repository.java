package codacy.challenge.vcs.domain;

import org.immutables.value.Value;

import java.nio.file.Path;

/**
 * The repository representation.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@Value.Immutable
public interface Repository {

    /**
     * The owner of the repository.
     *
     * @return The owner of the repository.
     */
    String getOwner();

    /**
     * The name of the repository.
     *
     * @return The name of the repository.
     */
    String getName();

    /**
     * The local path for the repository.
     *
     * @return The local path for the repository.
     */
    Path getLocalPath();
}
