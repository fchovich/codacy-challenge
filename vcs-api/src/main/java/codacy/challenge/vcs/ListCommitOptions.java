package codacy.challenge.vcs;

import codacy.challenge.vcs.domain.Repository;
import org.immutables.value.Value;

/**
 * The options for the {@link VCSCaller#listCommits(Repository, ListCommitOptions)}.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@Value.Immutable
public interface ListCommitOptions {

    /**
     * The size of the page to list.
     *
     * @return The size of the page to list.
     */
    @Value.Default
    default int getPageSize() {
        return 100;
    }

    /**
     * The page to list.
     *
     * @return The page to list.
     */
    @Value.Default
    default int getPage() {
        return 0;
    }
}
