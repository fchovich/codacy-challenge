package codacy.challenge.vcs;

import java.nio.file.Path;

/**
 * {@link Exception} for when the repository already exists.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public class RepositoryAlreadyExistsException extends Exception {

    /**
     * The path for the repository.
     */
    private final Path repositoryPath;

    /**
     * Default constructor.
     *
     * @param repositoryPath The path for the repository.
     * @param message        The exceptin message.
     */
    public RepositoryAlreadyExistsException(final Path repositoryPath, final String message) {
        super(message);
        this.repositoryPath = repositoryPath;
    }

    /**
     * Obtains the path for the repository.
     *
     * @return The path for the repository.
     */
    public Path getRepositoryPath() {
        return repositoryPath;
    }
}
