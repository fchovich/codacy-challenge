package codacy.challenge.fchovich.cli;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

/**
 * The process executor.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public class ProcessExecutor {

    /**
     * Executes the given command line in the given path.
     *
     * @param path    The path to run the given command in.
     * @param command The command to run in the given path.
     * @return The line iterator with the output of the process.
     */
    public LineIterator runProcess(final Path path, final String... command) {
        final ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.directory(path.toFile());

        try {
            final Process process = processBuilder.start();
            //TODO fchovich MAKE TIMEOUT CONFIGURABLE
            process.waitFor(60000, TimeUnit.MILLISECONDS);

            final InputStream inputStream = process.getInputStream();

            return IOUtils.lineIterator(inputStream, "UTF-8");
        } catch (IOException | InterruptedException e) {
            throw new IllegalStateException(String.format("Error running process: %s.", command), e);
        }
    }
}
