package codacy.challenge.commitretriever.web;

import codacy.challenge.commitretriever.CommitRetriever;
import codacy.challenge.commitretriever.CommitRetrieverOptions;
import codacy.challenge.commitretriever.ImmutableCommitRetrieverOptions;
import codacy.challenge.fchovich.cli.ProcessExecutor;
import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.git.GitApiVCSCaller;
import codacy.challenge.vcs.git.GitCliVCSCaller;
import codacy.challenge.vcs.git.http.GitHubAPI;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.Javalin;

import java.util.List;

/**
 * The web application for the {@link CommitRetriever}.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public class CommitRetrieverWebApp implements Runnable, AutoCloseable {

    /**
     * The object mapper responsible for marshaling and unmarshaling.
     */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    /**
     * The commit retriever.
     */
    private final CommitRetriever commitRetriever;

    /**
     * The web server.
     */
    private final Javalin app;

    /**
     * Default constructor.
     */
    public CommitRetrieverWebApp() {
        // TODO fchovich SETUP A PROPER FACTORY FOR DEPENDENCIES
        this.commitRetriever = new CommitRetriever(
                new GitApiVCSCaller(GitHubAPI.connect()),
                new GitCliVCSCaller(OBJECT_MAPPER, new ProcessExecutor())
        );
        this.app = Javalin.create().start(7000);
    }

    public static void main(String[] args) {
        new CommitRetrieverWebApp().run();
    }

    @Override
    public void run() {
        this.app.get("/commits", ctx -> {
            //TODO fchovich CREATE AN OPTION TO PARSE FROM AN URL THE OWNER AND NAME OF REPO

            final String owner = ctx.queryParam("owner");
            final String repo = ctx.queryParam("repo");

            //TODO fchovich CENTRALIZE DEFAULT VALUES
            final int page = Integer.parseInt(ctx.queryParam("page", "1"));
            final int pageSize = Integer.parseInt(ctx.queryParam("size", "100"));

            final CommitRetrieverOptions options = ImmutableCommitRetrieverOptions.builder()
                    .page(page)
                    .size(pageSize)
                    .build();

            final List<Commit> response = this.commitRetriever.listCommits(owner, repo, options);
            ctx.result(OBJECT_MAPPER.writeValueAsString(response));
        });
    }

    @Override
    public void close() throws Exception {
        //TODO fchovich ADD LOGGER
        this.app.stop();
    }
}
