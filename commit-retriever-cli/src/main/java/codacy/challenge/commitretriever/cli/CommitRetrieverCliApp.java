package codacy.challenge.commitretriever.cli;

import codacy.challenge.commitretriever.CommitRetriever;
import codacy.challenge.commitretriever.CommitRetrieverOptions;
import codacy.challenge.commitretriever.ImmutableCommitRetrieverOptions;
import codacy.challenge.fchovich.cli.ProcessExecutor;
import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.git.GitApiVCSCaller;
import codacy.challenge.vcs.git.GitCliVCSCaller;
import codacy.challenge.vcs.git.http.GitHubAPI;
import com.fasterxml.jackson.databind.ObjectMapper;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.util.List;

/**
 * The CLI application for the {@link CommitRetriever}.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@Command(name = "commit-retriever",
        description = "Retrieves commits from a given URL.",
        mixinStandardHelpOptions = true,
        version = "1.0")
public class CommitRetrieverCliApp implements Runnable {

    /**
     * The commit retriever.
     */
    private final CommitRetriever commitRetriever;

    //TODO fchovich CREATE AN OPTION TO PARSE FROM AN URL THE OWNER AND NAME OF REPO

    @Option(names = "--size", description = "The number of commits to retrieve. Default: 100.", defaultValue = "100")
    private int pageSize;

    @Option(names = "--page", description = "The number of commits to offset. Default: 1.", defaultValue = "1")
    private int page;

    @Parameters(paramLabel = "OWNER", index = "0", description = "The owner of the repository to list the commits from.")
    private String owner;

    @Parameters(paramLabel = "REPO", index = "1", description = "The name of the repository to list the commits from.")
    private String repo;

    /**
     * Default constructor.
     *
     * @param commitRetriever The commit retriever.
     */
    public CommitRetrieverCliApp(final CommitRetriever commitRetriever) {
        this.commitRetriever = commitRetriever;
    }

    @Override
    public void run() {
        System.out.printf("Listing commits for %s/%s\n", this.owner, this.repo);
        System.out.printf("\tOptions - page: %s, size: %s\n", this.page, this.pageSize);

        //TODO fchovich CENTRALIZE DEFAULT VALUES
        final CommitRetrieverOptions options = ImmutableCommitRetrieverOptions.builder()
                .page(this.page)
                .size(this.pageSize)
                .build();
        final List<Commit> commits;
        commits = this.commitRetriever.listCommits(this.owner, this.repo, options);
        commits.forEach(System.out::println);
    }

    public static void main(String[] args) {
        // TODO fchovich SETUP A PROPER FACTORY FOR DEPENDENCIES
        final CommitRetriever commitRetriever = new CommitRetriever(
                new GitApiVCSCaller(GitHubAPI.connect()),
                new GitCliVCSCaller(new ObjectMapper(), new ProcessExecutor())
        );

        CommandLine.run(new CommitRetrieverCliApp(commitRetriever), args);
    }
}
