package codacy.challenge.vcs.git;

import codacy.challenge.fchovich.cli.ProcessExecutor;
import codacy.challenge.vcs.ListCommitOptions;
import codacy.challenge.vcs.RepositoryAlreadyExistsException;
import codacy.challenge.vcs.VCSCaller;
import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.domain.ImmutableRepository;
import codacy.challenge.vcs.domain.Repository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.LineIterator;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * The {@link VCSCaller} for the command-line GitHub.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public class GitCliVCSCaller implements VCSCaller {

    /**
     * The object mapper responsible for marshaling/unmarshaling of JSON/Objects.
     */
    private final ObjectMapper objectMapper;

    /**
     * The executor of command line.
     */
    private final ProcessExecutor processExecutor;

    /**
     * Default constructor.
     *
     * @param objectMapper    The object mapper.
     * @param processExecutor The executor of command line.
     */
    public GitCliVCSCaller(final ObjectMapper objectMapper, final ProcessExecutor processExecutor) {
        this.objectMapper = objectMapper;
        this.processExecutor = processExecutor;
    }

    @Override
    public Repository clone(final Path basePath,
                            final String owner,
                            final String remoteName,
                            final String localName) throws RepositoryAlreadyExistsException {
        checkNotNull(basePath, "The base path is mandatory.");
        checkArgument(isNotBlank(owner), "The owner of the repository is mandatory (%s).", owner);
        checkArgument(isNotBlank(remoteName), "The remote name of the repository is mandatory (%s).", remoteName);
        checkArgument(isNotBlank(localName), "The local name of the repository is mandatory (%s).", localName);

        final Path repositoryPath = basePath.resolve(localName);
        if (repositoryPath.toFile().exists()) {
            throw new RepositoryAlreadyExistsException(
                    repositoryPath,
                    String.format("Repository with name '%s' already exists.", localName)
            );
        }

        final URL url = createCloningUrl(owner, remoteName);

        this.processExecutor.runProcess(basePath, "git", "clone", url.toString(), localName);

        return ImmutableRepository.builder()
                .owner(owner)
                .name(remoteName)
                .localPath(repositoryPath)
                .build();
    }

    /**
     * Creates the URL to clone the repository from. It will substitute the owner and repository name into
     * "https://github.com/{owner}/{name}.git".
     *
     * @param owner The owner of the repository.
     * @param name  The name of the repository.
     * @return The URL.
     */
    private static URL createCloningUrl(final String owner, final String name) {
        final String url = String.format("https://github.com/%s/%s.git", owner, name);
        try {
            return new URL(url);
        } catch (final MalformedURLException e) {
            throw new IllegalArgumentException(String.format("Error creating URL for repository: %s.", url), e);
        }
    }

    @Override
    public List<Commit> listCommits(final Repository repository, final ListCommitOptions options) {
        checkNotNull(repository.getLocalPath(), "The local path is mandatory for listing commits.");
        checkArgument(options.getPage() > 0, "The page has to be a number greater than zero.");
        checkArgument(options.getPageSize() > 0, "The page size has to be a number greater than zero.");

        final LineIterator lineIterator = this.processExecutor.runProcess(
                repository.getLocalPath(),
                "git",
                "--no-pager",
                "log",
                "--pretty=tformat:{\"hash\":\"%H\",\"commiterEmail\":\"%ce\",\"date\":%ct}",
                String.format("--max-count=%s", options.getPageSize()),
                String.format("--skip=%s", (options.getPage() - 1) * options.getPageSize())
        );

        return parseCommits(this.objectMapper, lineIterator);
    }

    /**
     * Parse the commits from the given line iterator with the given object mapper.
     * Each line of the line iterator represents a commit and is serialized separately.
     *
     * @param objectMapper The object mapper to serialize the commits in the line iterator.
     * @param lineIterator The line iterator containing one commit per line.
     * @return The list of the commits parsed.
     */
    private static List<Commit> parseCommits(final ObjectMapper objectMapper, final LineIterator lineIterator) {
        final List<Commit> commits = new ArrayList<>();
        while (lineIterator.hasNext()) {
            final String commit = lineIterator.next();
            try {
                commits.add(objectMapper.readValue(commit, Commit.class));
            } catch (final IOException e) {
                throw new IllegalStateException(String.format("Error parsing commit log: %s.", commit), e);
            }
        }
        return commits;
    }
}
