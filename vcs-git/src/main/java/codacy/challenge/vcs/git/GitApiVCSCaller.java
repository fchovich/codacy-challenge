package codacy.challenge.vcs.git;

import codacy.challenge.vcs.ListCommitOptions;
import codacy.challenge.vcs.VCSCaller;
import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.domain.ImmutableCommit;
import codacy.challenge.vcs.domain.Repository;
import codacy.challenge.vcs.git.http.GitHubAPI;
import codacy.challenge.vcs.git.http.GitHubCommit;

import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * The {@link VCSCaller} for the {@link GitHubAPI}.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public class GitApiVCSCaller implements VCSCaller {

    /**
     * The GitHub API.
     */
    private GitHubAPI gitHubAPI;

    /**
     * Default constructor.
     *
     * @param gitHubAPI The GitHub API.
     */
    public GitApiVCSCaller(final GitHubAPI gitHubAPI) {
        this.gitHubAPI = gitHubAPI;
    }

    @Override
    public Repository clone(final Path basePath,
                            final String owner,
                            final String remoteName,
                            final String localName) {
        throw new UnsupportedOperationException("It is not possible to clone a repository through the GitHub API.");
    }

    @Override
    public List<Commit> listCommits(final Repository repository, final ListCommitOptions options) {
        checkArgument(isNotBlank(repository.getOwner()), "The owner of the repository is mandatory.");
        checkArgument(isNotBlank(repository.getName()), " The remote name of the repository is mandatory.");

        final List<GitHubCommit> commits = this.gitHubAPI.listCommits(
                repository.getOwner(),
                repository.getName(),
                options.getPage(),
                options.getPageSize()
        );
        return commits.stream()
                .map(GitApiVCSCaller::convert)
                .collect(Collectors.toList());
    }

    /**
     * Converts the {@link GitHubCommit} into the {@link Commit} representation of the {@link VCSCaller}.
     *
     * @param commit The GitHub commit.
     * @return The {@link Commit} representation of the {@link VCSCaller}.
     */
    private static Commit convert(final GitHubCommit commit) {
        return ImmutableCommit.builder()
                .hash(commit.getSha())
                .commiterEmail(commit.getCommit().getCommitter().getEmail())
                .date(Instant.parse(commit.getCommit().getCommitter().getDate()).toEpochMilli())
                .build();
    }
}
