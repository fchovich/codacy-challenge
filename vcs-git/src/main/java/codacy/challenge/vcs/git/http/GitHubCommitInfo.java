package codacy.challenge.vcs.git.http;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * The GitHub commit general information representation.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@JsonSerialize(as = ImmutableGitHubCommitInfo.class)
@JsonDeserialize(as = ImmutableGitHubCommitInfo.class)
@Value.Immutable
public interface GitHubCommitInfo {

    /**
     * The commiter.
     *
     * @return The commiter.
     */
    GitHubCommitter getCommitter();
}
