package codacy.challenge.vcs.git.http;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * The GitHub Commit representation.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@JsonSerialize(as = ImmutableGitHubCommit.class)
@JsonDeserialize(as = ImmutableGitHubCommit.class)
@Value.Immutable
public interface GitHubCommit {

    /**
     * The SHA-1 hex.
     *
     * @return The SHA-1 hex.
     */
    String getSha();

    /**
     * The commit general information.
     *
     * @return The commit general information.
     */
    GitHubCommitInfo getCommit();
}
