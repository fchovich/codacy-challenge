package codacy.challenge.vcs.git.http;

import feign.Feign;
import feign.Logger;
import feign.Param;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

import java.util.List;

/**
 * The GitHub API.
 * Follows the pattern proposed by the Feign library.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
public interface GitHubAPI {

    /**
     * List the commits of the repository.
     *
     * @param owner   The owner of the repository.
     * @param name    The name of the repository.
     * @param page    The page to list.
     * @param perPage The size of the page to list.
     * @return
     */
    @RequestLine("GET /repos/{owner}/{name}/commits?page={page}&per_page={perPage}")
    List<GitHubCommit> listCommits(@Param("owner") String owner,
                                   @Param("name") String name,
                                   @Param("page") int page,
                                   @Param("perPage") int perPage);

    /**
     * Responsible to instantiate {@link GitHubAPI}.
     *
     * @return The GitHub API.
     */
    //TODO fchovich CREATE A PROPER FACTORY
    static GitHubAPI connect() {
        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                // TODO fchovich IMPLEMENT ERROR DECODER - ANY ERROR RETRIES A FEW TIMES
                //.errorDecoder(new GitHubErrorDecoder(decoder))
                .logger(new Logger.ErrorLogger())
                .logLevel(Logger.Level.BASIC)
                .target(GitHubAPI.class, "https://api.github.com");
    }
}
