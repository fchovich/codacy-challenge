package codacy.challenge.vcs.git.http;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * The GitHub commiter representation.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@JsonSerialize(as = ImmutableGitHubCommitter.class)
@JsonDeserialize(as = ImmutableGitHubCommitter.class)
@Value.Immutable
public interface GitHubCommitter {

    /**
     * The committer's email.
     *
     * @return The committer's email.
     */
    String getEmail();

    /**
     * The commiter's date.
     *
     * @return The commiter's date.
     */
    String getDate();
}
