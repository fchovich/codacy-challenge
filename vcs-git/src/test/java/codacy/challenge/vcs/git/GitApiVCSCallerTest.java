package codacy.challenge.vcs.git;

import codacy.challenge.vcs.ImmutableListCommitOptions;
import codacy.challenge.vcs.ListCommitOptions;
import codacy.challenge.vcs.domain.Commit;
import codacy.challenge.vcs.domain.ImmutableCommit;
import codacy.challenge.vcs.domain.ImmutableRepository;
import codacy.challenge.vcs.domain.Repository;
import codacy.challenge.vcs.git.http.GitHubAPI;
import codacy.challenge.vcs.git.http.GitHubCommit;
import codacy.challenge.vcs.git.http.GitHubCommitInfo;
import codacy.challenge.vcs.git.http.GitHubCommitter;
import codacy.challenge.vcs.git.http.ImmutableGitHubCommit;
import codacy.challenge.vcs.git.http.ImmutableGitHubCommitInfo;
import codacy.challenge.vcs.git.http.ImmutableGitHubCommitter;
import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link GitApiVCSCaller}.
 *
 * @author Fernando Chovich (fernandochovich@gmail.com)
 * @since 1.0.0
 */
@SuppressWarnings("UnusedLabel") // USED FOR BDD BLOCKS
@RunWith(MockitoJUnitRunner.class)
public class GitApiVCSCallerTest {

    /**
     * The mocked {@link GitHubAPI}, dependency of {@link #vcs}.
     */
    @Mock
    private GitHubAPI gitHubAPI;

    /**
     * The unit under test.
     */
    @InjectMocks
    private GitApiVCSCaller vcs;

    /**
     * Verifies that all mocks were verified.
     */
    @After
    public void verifyMocks() {
        Mockito.verifyNoMoreInteractions(gitHubAPI);
    }

    /**
     * Checks that is not possible for {@link #vcs}O to clone.
     */
    @Test
    public void cloningIsNotPossible() {
        final AbstractThrowableAssert<?, ? extends Throwable> expectedException;

        when:
        expectedException = assertThatThrownBy(() -> this.vcs.clone(null, null, null, null));

        then:
        expectedException
                .as("There is no need to clone the repository to perform REST calls.")
                .isInstanceOf(UnsupportedOperationException.class)
                .hasMessage("It is not possible to clone a repository through the GitHub API.");
    }

    /**
     * Checks that commits are retrieved successfully by the {@link #vcs}.
     */
    @Test
    public void commitsAreRetrieved() {
        final Repository repository;
        final ListCommitOptions listOptions;
        final List<Commit> actualCommits, expectedCommits;
        final List<GitHubCommit> remoteCommits;
        final Instant firstDate, secondDate;

        given:
        repository = ImmutableRepository.builder()
                .owner("fchovich")
                .name("codacy-challenge")
                .localPath(Paths.get("/tmp"))
                .build();

        listOptions = ImmutableListCommitOptions.builder()
                .build();

        firstDate = Instant.now();
        secondDate = Instant.now();

        remoteCommits = Arrays.asList(
                createGitHubCommit("11", "a@gmail.com", firstDate.toString()),
                createGitHubCommit("22", "b@gmail.com", secondDate.toString())
        );

        orchestrate:
        when(this.gitHubAPI.listCommits(
                repository.getOwner(),
                repository.getName(),
                listOptions.getPage(),
                listOptions.getPageSize()
        )).thenReturn(remoteCommits);

        when:
        actualCommits = this.vcs.listCommits(repository, listOptions);

        then:
        expectedCommits = Arrays.asList(
                ImmutableCommit.builder().hash("11").commiterEmail("a@gmail.com").date(firstDate.toEpochMilli()).build(),
                ImmutableCommit.builder().hash("22").commiterEmail("b@gmail.com").date(secondDate.toEpochMilli()).build()
        );

        assertThat(actualCommits)
                .as("The commits should be converted.")
                .hasSameElementsAs(expectedCommits)
                .hasSize(2);

        and:
        verify(this.gitHubAPI, only()).listCommits(
                repository.getOwner(),
                repository.getName(),
                listOptions.getPage(),
                listOptions.getPageSize()
        );
    }

    /**
     * Creates a {@link GitHubCommit} with the given information.
     *
     * @param sha   The SHA-1 hex.
     * @param email The committer's email.
     * @param date  The commit's date.
     * @return The commit.
     */
    //TODO fchovich isolate this to a class that generates commits from normalized information
    private static GitHubCommit createGitHubCommit(final String sha, final String email, final String date) {
        final GitHubCommitter committer = ImmutableGitHubCommitter.builder()
                .email(email)
                .date(date)
                .build();
        final GitHubCommitInfo commitInfo = ImmutableGitHubCommitInfo.builder()
                .committer(committer)
                .build();
        return ImmutableGitHubCommit.builder()
                .sha(sha)
                .commit(commitInfo)
                .build();
    }
}
